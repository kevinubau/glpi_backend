var mysql = require('mysql')
var express = require('express')
var moment = require('moment')
var app = express()
var port = 1152
var tableNames = ['glpi_computers', 'glpi_peripherals', 'glpi_monitors']
const prefix = 'prof'

app.listen(port)

app.all('/*', function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
})

var con = mysql.createConnection({
  host: "172.24.160.5",
  user: "usr_consultaApp",
  password: "12345",
  database: "ActivosComputacion",
  port: '3307'
})

con.connect()

app.get('/search/:id', function (req, res) {
 
  check(req).then((resp)=>{
    res.send(resp)
  })
  

});

app.get('/horario', function (req, res) {
  
  return new Promise(function (resolve, reject) {
   const text = `select re.comment, re.begin, re.end, pe.name  from glpi_reservations as
  re inner join glpi_reservationitems as reitem on re.reservationitems_id = reitem.id inner
  join glpi_peripherals as pe on reitem.items_id = pe.id where pe.name NOT LIKE '${prefix}%' and re.begin between
  '${getMonday()}%' and '${getSunday()}%' order by re.begin`

    console.log(text)
      con.query(text, (err, dbres) => {
        if (err) {
          return reject(err)
  
        }
        dbres.forEach(reservation => {
          if(reservation.comment){
              reservation.beginH = moment(reservation.begin).format('LT')
              reservation.endH = moment(reservation.end).format('LT')
          }
        })
        res.send(dbres)
      })
  })
})

app.get('/horarioprofesores', function (req, res) {
  
  return new Promise(function (resolve, reject) {
   const text = `select re.comment, re.begin, re.end, pe.name from glpi_reservations as re
    inner join glpi_reservationitems as reitem on re.reservationitems_id = reitem.id inner join
    glpi_peripherals as pe on reitem.items_id = pe.id where pe.name LIKE '${prefix}%' and re.begin between
    '${getMonday()}%' and '${getSunday()}%' order by re.begin`

    console.log(text)
      con.query(text, (err, dbres) => {
        if (err) {
          return reject(err)
  
        }
        dbres.forEach(reservation => {
          if(reservation.comment){
              reservation.beginH = moment(reservation.begin).format('LT')
              reservation.endH = moment(reservation.end).format('LT')
          }
        })
        res.send(dbres)
      })
  })
})

function getLastRecord(name, id) {
  return new Promise(function (resolve, reject) {
    const text = "SELECT c.name as dispname, c.comment, c.template_name, l.name, l.completename FROM " + name + " as c INNER JOIN glpi_locations as l ON c.locations_id = l.id where c.name = '" + id + "'";
    const values = [id]
    con.query(text, values, (err, dbres) => {
      if (err) {
        return reject(err)

      }
      resolve(dbres)
    })
  })
}

async function check(req) {

  var id = req.params.id
  var newObject = {}
  const tablesLength = tableNames.length
  for (let index = 0; index < tablesLength; index++) {
    console.log(tableNames[index])

    await getLastRecord(tableNames[index], id).then(function (rows) {

      if (rows.length > 0) {
        newObject.dispId = rows[0].dispname
        newObject.comment = rows[0].comment
        newObject.template = rows[0].template_name
        newObject.smallLocation = rows[0].name
        newObject.fullLocation = rows[0].completename
        console.log(rows[0])
        index =  tablesLength
      }

    }).catch((err) => setImmediate(() => {
      throw err
    }))
  }
  return newObject
}



function getMonday() {
  console.log(moment().isoWeekday(1).format().split("T")[0]);
  return moment().isoWeekday(1).format().split("T")[0]
}

function getSunday()
{
  
  return moment().isoWeekday(7).format().split("T")[0]
  
}

console.log(getMonday(new Date()))
console.log(getSunday(new Date()))
