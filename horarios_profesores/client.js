window.main =  function main(){
    const Http = new XMLHttpRequest();
    //const url='http://172.24.160.5:1152/horario';
    const url=`http://${window.location.hostname}:1152/horarioprofesores`;
    Http.open("GET", url);
    Http.send();
    Http.onloadend=(e)=>{
        localStorage.clear()
        localStorage.setItem('data', (Http.responseText));
        test();
    }
    var lab  = JSON.parse(localStorage.getItem('lab'));
    if(lab){
        document.getElementById("dropdownMenuButton").innerHTML  = lab;
    }
    
    
}

function fillSelect(data){
    let labs = []
    data.forEach(element => {
        if(!labs.includes(element.name)){
            labs.push(element.name)
            newOption = document.createElement("a")
            select = document.getElementById("custom-select")
            newOption.classList.add("dropdown-item")
            newOption.addEventListener("click", function(){updateLab(element.name)})
            newOption.text = element.name;
            select.appendChild(newOption)
        }
    });
    return labs
}

function updateLab(lab){
    localStorage.setItem('lab', lab);
    document.getElementById("dropdownMenuButton").innerHTML  = lab;
    test()
}

function test(){
    $('#tabletest tr td').remove();
    var data  = JSON.parse(localStorage.getItem('data'));
    let table = document.getElementById("tabletest");
    let pivot = data[0].begin
    const ids = ['l', 'k', 'm', 'j', 'v', 's']
    let index = 1
    
    let m = []
    let k = []
    let w = []
    let t = []
    let f = []
    let s = []
    
    let week = []
    if(localStorage.getItem('lab') == null){
        var labs = fillSelect(data)
        updateLab(labs[0])
        $('#tabletest tr td').remove();
    }
    data.forEach(scheduleItem => {
        if(pivot.split("T")[0] != scheduleItem.begin.split("T")[0]){
            
            index+=1
            pivot =scheduleItem.begin
        }
        if(scheduleItem.comment != "" && scheduleItem.name === localStorage.getItem('lab')){
            
            
            switch(index) {
                case 1:
                  m.push(scheduleItem)
                  break;
                case 2:
                k.push(scheduleItem)
                  break;
                case 3:
                w.push(scheduleItem)
                    break;
                case 4:
                    t.push(scheduleItem)
                    break;
                case 5:
                    f.push(scheduleItem)
                    break;
                case 6:
                    s.push(scheduleItem)
                    break;
                
                default:
                  
              }
        }
        
    });
   
    week.push(m)
    week.push(k)
    week.push(w)
    week.push(t)
    week.push(f)
    week.push(s)
   
    let max = Math.max.apply(Math, week.map(function (el) { return el.length }))-1;
    let counter = 0;
    for (let index = 0; index < max+1; index++) {
        
        
        let trow = document.createElement("tr");



        for (let index2 = 0; index2 < 6; index2++) {
            let day = week[index2][counter];
            let div = document.createElement("div");
            let span = document.createElement("span");
            span.classList.add("span");

            if(day){
                let scheduleItem = day;
                let td = document.createElement("td");
                
                let span2 = document.createElement("span");
                
                textnode1=document.createTextNode(` ${scheduleItem.comment.toUpperCase()} `);
                textnode2=document.createTextNode(`${scheduleItem.beginH} - ${scheduleItem.endH} `);
                
                div.classList.add("scheduleItem");
                span.appendChild(textnode1);
                span2.appendChild(textnode2);
                div.appendChild(span);
                div.appendChild(span2);
                td.appendChild(div);
                trow.appendChild(td);
            }
            else{
                div.classList.add("scheduleItem");
                div.classList.add("available");
                let td = document.createElement("td");
                textnode1=document.createTextNode("--");
                span.appendChild(textnode1);
                div.appendChild(span);
                td.appendChild(div);
               
                trow.appendChild(td);
            }
    
            table.appendChild(trow);
            
           
        }
        counter++
    }
        
            
    
        
        
 
    


}