const path = require('path');

module.exports = {
  entry: './client.js',
  output: {
    filename: 'main.js',
    path: path.join(__dirname, '/')
  },
  module: {
    rules: [
      { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
    ]
  }
};
